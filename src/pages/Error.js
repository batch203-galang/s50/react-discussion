import Banner from "../components/Banner";

export default function Error() {
	
	const data = {
		title: "404 - Not Found",
		description: "The page you are looking for can not be found",
		destination: "/",
		label: "Back to home"
	}

	return(
		<Banner bannerProp={data} />
	)
}