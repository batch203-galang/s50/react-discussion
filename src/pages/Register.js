import {useState, useEffect, useContext} from "react";

import {Navigate, useNavigate} from "react-router-dom";
import {Form, Button} from "react-bootstrap";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function Register(){

	// Allow us to consume the User Context object and its values(properties) to use for user validation.
	const {user} = useContext(UserContext);

	const navigate = useNavigate();
	
	// Create state hooks to store the values of the input fields
	const [fName, setFName] = useState('');
	const [lName, setLName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	// create a state to determine whether the submit button is enabled or not.
	const [isActive, setIsActive] = useState(false);

	/*
		Two way binding
			- is done so that we can assure that we can save the input into our states as we type into the input elements. This is so we don't have to save it just before submit.

			e.target = current element where the event happened.
			e.target.value = current value of the element where the event happened.
	*/

	// check if the values are successfully binded
		// console.log(fName);
		// console.log(lName);
		// console.log(email);
		// console.log(mobileNo);
		// console.log(password1);
		// console.log(password2);
		
	useEffect(()=>{

		// Enable the button if:
			// All the fields are populated
			// Both paswords match
		if((fName !== '' && lName !== '' && email !== '' && mobileNo !== '' && password1 !== '' && password2 !== '')&&(password1 === password2)) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [fName, lName, email, mobileNo, password1, password2])

	// Function to simulate user registration
	function registerUser(e) {
		e.preventDefault(); // prevents page loading/redirection via form submission

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify ({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Kindly provide another email to complete the registration."
				})
			}
			else {
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: fName,
						lastName: lName,
						email: email,
						mobileNumber: mobileNo,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data) {
						Swal.fire({
							title: "Registration Successful",
							icon: "success",
							text: "Welcome to Zuitt!"
						})

						// Clear input fields
							setFName('');
							setLName('');
							setEmail('');
							setMobileNo('');
							setPassword1('');
							setPassword2('');

							// Allow us to redirect the user to the login page after account registration
							navigate("/login");
					}
					else {
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						})
					}
				})
			}
		})


		// Notify user for registration
		// alert("Thank you, goodbye!");

	}

	return(
		// (user.id != null)
		// ?
		// 	<Navigate to="/courses" />
		// :
		<>
			<h1 className="my-5 text-center">Register</h1>
			<Form onSubmit={e => registerUser(e)}>

			 <Form.Group className="mb-3" controlId="firstName">
		        <Form.Label>First Name</Form.Label>
		        <Form.Control 
		        	type="text" 
		        	placeholder="Enter first name" 
		        	value={fName}
		        	onChange={e => setFName(e.target.value)}
		        	required
		        />
		     </Form.Group>

			  <Form.Group className="mb-3" controlId="lastName">
		        <Form.Label>Last Name</Form.Label>
		        <Form.Control 
		        	type="text" 
		        	placeholder="Enter last name"
		        	value={lName} 
		        	onChange={e => setLName(e.target.value)}
		        	required
		        />
		     </Form.Group>



		      <Form.Group className="mb-3" controlId="emailAddress">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control 
		        	type="email" 
		        	placeholder="Enter email"
		        	value={email}
		        	onChange={e => setEmail(e.target.value)}
		        	required
		         />
		        <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="mobileNo">
		        <Form.Label>Mobile number</Form.Label>
		        <Form.Control 
		        	type="text" 
		        	placeholder="09xx-xxx-xxxx"
		        	value={mobileNo}
		        	onChange={e => setMobileNo(e.target.value)} 
		        	required
		        />
		     </Form.Group>

		      <Form.Group className="mb-3" controlId="password1">
		        <Form.Label>Password</Form.Label>
		        <Form.Control 
		        	type="password" 
		        	placeholder="Enter password"
		        	value={password1}
		        	onChange={e => setPassword1(e.target.value)} 
		        	required 
		        />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password2">
		        <Form.Label>Confirm Password</Form.Label>
		        <Form.Control 
		        	type="password" 
		        	placeholder="Confirm password"
		        	value={password2}
		        	onChange={e => setPassword2(e.target.value)}
		        	required 
		        />
		      </Form.Group>
		  	  {/* Conditional Rendering - submit button will be active based on the isActive state*/}
		  	  {
		  	  	isActive 
		  	  	?
			  	  	<Button variant="primary" type="submit" id="submitBtn">
			        Submit
			      	</Button>
			    :
			    	<Button variant="danger" type="submit" id="submitBtn" disabled>
			        Submit
			      	</Button>
		  	  }
		      
		    </Form>
		</>
	)
}