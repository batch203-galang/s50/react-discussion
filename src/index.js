import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

// Import a Bootstrap 5 CSS
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>

  // React.StrictMode is a built-in react component which is used to higlight potential error
);

/*
  root.render() allows us to render/display our reactjs elements and show it in our HTML document.
    root.render(<reactElement>)
*/

/*
  ReactJS Component
  - This are reusable parts of our react application.
  - They are independent UI parts of our app.
  - Components are functions that return react elements
  - Components naming Convention: PascalCase
    - Capitalized letter for all words of the function name AND file name associtated with it.

*/

// const name = "John Smith";
//const element = <h1>Hello, {name}</h1>

// const user = {
//   firstName: "Jane",
//   lastName: "Smith"
// }

// function formatName(fullName) {
//   return `${fullName.firstName} ${fullName.lastName}`
// }

  // The "h1" tag is an example of what call JSX.
  // It allows us to create HTML elements and at the same time allows us to apply JavaScript code to our elements.
  // JSX makes it easier to write both HTML and JavaScript code in a single file as opposed to creating seperate files (One for HTML and one for JavaScript)

// const element = <h1>Hello, {formatName(user)}</h1>

// root.render(element);

// React Bootstrap Component
/*
  Syntax:
    import { moduleName/s} from "file path"
    
*/