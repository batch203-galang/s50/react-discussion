import {useState, useEffect} from "react";

import {Card, Button} from "react-bootstrap";
import {Link} from "react-router-dom";
                                // destructure the "courseProp" from the prop parameter
                                // CourseCard(prop)

export default function CourseCard({courseProp}) {

    // console.log(props.courseProp.name);
    // console.log(typeof props);
    // console.log(courseProp);

    // Scenario: Keep track the number of enrollees of each course.


    // Destructure the course properties 
    const { _id, name, description, price, slots } = courseProp;

    // Syntax:
        // const [stateName, setStateName] = useState(initialStateValue);
            // Using the state hook, it returns an array with the following elements:
                // first element contains the current initial State value.
                // second element is a function that is used to change the value of the first element. 
        // const [count, setCount] = useState(0);
        // console.log(useState(10));

        // Use state hook for getting the seats for this course
        // const [seats, setSeat] = useState(30);

        // Function that keeps track of the enrollees for a course

        /*
            We will refactor the "enroll" function using the "useEffect" hooks to disable the enroll button when the seats reach zero.
        */

        // Use the disabled state to disable the enroll button
        // const [disabled, setDisabled] = useState(false);

        // Syntax:
            // useEffect(function, [dependencyArray])

        // function unEnroll(){
        //     setCount(count - 1);
        // }

        // function enroll(){
            // Activity code
            // if(count === 30){
            //     alert("Sorry, there's no more seats available.");
            // }
            // else {
            //         // 0 + 1
            //         // setCount(1)
            //     setCount(count + 1);
            //     console.log(`Enrollees: ${count}`);

            //     setSeat(seat - 1);
            //     console.log(`Seats: ${seat}`)

            // }
            // End of Activity code

                    // 0 + 1
                    // setCount(1)
        //         setCount(count + 1);
        //         console.log(`Enrollees: ${count}`);

        //         setSeat(seats - 1);
        //         console.log(`Seats: ${seats}`)
        // }

        // useEffect(()=>{
        //     if(seats <= 0){
        //         setDisabled(true);
        //         alert("No more seats available.");
        //     }
        // },[seats]);

	return (
		<Card className="my-3">
            <Card.Body>
             <Card.Title>
                {name}
             </Card.Title>
                        
             <Card.Subtitle>
                Description:
             </Card.Subtitle>
             <Card.Text>
                {description}
             </Card.Text>
                         
             <Card.Subtitle>
                Price:
             </Card.Subtitle>
             <Card.Text>
                {price}
             </Card.Text>

             <Card.Subtitle>
                Slots:
             </Card.Subtitle>
             <Card.Text>
                {slots} available
             </Card.Text>

             <Button as={Link} to={`/courses/${_id}`} variant="primary">Details</Button>
            </Card.Body>
        </Card>
                
	)
}